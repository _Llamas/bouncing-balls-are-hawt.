class Ball{
  float ballX;
  float ballY;
  int bStroke = 2;
  float Xspeed;
  float Yspeed;
  float bSize;
  color bColor;
  Ball(int bX, int bY, float Xs, float Ys, float bs, color bc){
    this.ballX = bX;
    this.ballY = bY;
    this.Xspeed = Xs;
    this.Yspeed = Ys;
    this.bSize = bs;
    this.bColor = bc;
    
  }
  
  void move(){
    this.ballX = this.ballX + this.Xspeed;
    this.ballY = this.ballY + this.Yspeed;
  }
  
  void bounce(){
    if ((this.ballX - this.bSize/2 > width) || (this.ballX - this.bSize/2 < 0)){
      this.Xspeed = this.Xspeed * -1;
      bounceNum += 1;
    }
    if ((this.ballX + this.bSize/2 > width) || (this.ballX + this.bSize/2 < 0)){
      this.Xspeed = this.Xspeed * -1;
      bounceNum += 1;
    }
    
    if ((this.ballY - this.bSize/2 > width) || (this.ballY - this.bSize/2 < 0)){
      this.Yspeed = this.Yspeed * -1;
      bounceNum += 1;
    }
    if ((this.ballY + this.bSize/2 > width) || (this.ballY + this.bSize/2 < 0)){
      this.Yspeed = this.Yspeed * -1;
      bounceNum += 1;
    }
  }
  
  void display(){
    text(bounceNum, 10, 390);
    fill(255, 10); 
    rect(0-10, 0-10, width+10, height+10);
    stroke(bStroke);
    fill(bColor);
    ellipse(ballX, ballY, bSize, bSize);
    
  }
}

    
  
