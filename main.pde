Ball ball1, ball2, ball3;
int bounceNum;
void setup(){
  size(400, 400);
  smooth();
  background(255);
  ball1 = new Ball(80, 80, 2, 6.6, 60, color(133));
  ball2 = new Ball(200, 200, 4, -12.12, 60, color(210));
  ball3 = new Ball(305, 150, 1, 3.3, 60, color(44));
 
}

void draw(){
  
  
  ball1.move();
  ball1.bounce();
  ball1.display();
  ball2.move();
  ball2.bounce();
  ball2.display();
  ball3.move();
  ball3.bounce();
  ball3.display();
  
  
  }

  
  
  
